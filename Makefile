SCRIPTS_DIR := ~/.local/bin/scripts
AMBIG_SCRIPTS := barmenu barmenu_run slockd
WM_SCRIPTS := bspt herbsttags hwinmv
INTER_SCRIPTS := browser bsc dmpv fdc ffc lwc mprisctl qbc
BLOCKS_SCRIPTS := battery clock cpu disk internet memory mpdup music sigdwmb volume
GENERAL_SCRIPTS := crcparse gettags notify-fw notify-sound.sh songgrab sortsongs startvm startxw tagimg tagmp3 todo toggle
XORG_SCRIPTS := launch_polybar.sh liberclip monattach mondetach toggletouchpad tsoff tson xidlechk xidletog
WAY_SCRIPTS := swayidlechk swayidletog waytoggle
RUNIT_SCRIPTS := svlogc

all:

install: ambig-scripts wm-scripts inter-scripts blocks-scripts general-scripts xorg-scripts way-scripts runit-scripts
	@echo "Copying scripts to the appropriate dir..."

ambig-scripts: $(AMBIG_SCRIPTS)
ifdef AMBIG_SCRIPTS
	@echo "Installing 'ambiguous' scripts (scripts that run one option or the other, depending on display server)..."
	@mkdir -p $(SCRIPTS_DIR)
	@$(foreach script,$(AMBIG_SCRIPTS),\
		$(shell cp $(script) $(SCRIPTS_DIR)))
	@echo "Done."
endif

wm-scripts: $(WM_SCRIPTS)
ifdef WM_SCRIPTS
	@echo "Installing wm scripts, which are specific to window managers..."
	@mkdir -p $(SCRIPTS_DIR)
	@$(foreach script,$(WM_SCRIPTS),\
		$(shell cp $(script) $(SCRIPTS_DIR)))
	@echo "Done."
endif

inter-scripts: $(INTER_SCRIPTS)
ifdef INTER_SCRIPTS
	@echo "Installing 'interface' scripts (scripts that use barmenu as an interface to open another application)..."
	@mkdir -p $(SCRIPTS_DIR)
	@$(foreach script,$(INTER_SCRIPTS),\
		$(shell cp $(script) $(SCRIPTS_DIR)))
	@echo "Done."
endif

blocks-scripts: $(BLOCKS_SCRIPTS)
ifdef BLOCKS_SCRIPTS
	@echo "Installing 'dwmblocks' scripts (scripts that are modules for dwmblocks)..."
	@mkdir -p $(SCRIPTS_DIR)
	@$(foreach script,$(BLOCKS_SCRIPTS),\
		$(shell cp $(script) $(SCRIPTS_DIR)))
	@echo "Done."
endif

general-scripts: $(GENERAL_SCRIPTS)
ifdef GENERAL_SCRIPTS
	@echo "Installing 'general' scripts (scripts that don't depend on my other scripts)..."
	@mkdir -p $(SCRIPTS_DIR)
	@$(foreach script,$(GENERAL_SCRIPTS),\
		$(shell cp $(script) $(SCRIPTS_DIR)))
	@echo "Done."
endif

xorg-scripts: $(XORG_SCRIPTS)
ifdef XORG_SCRIPTS
	@echo "Installing 'xorg' scripts (scripts designed for use with xorg)..."
	@mkdir -p $(SCRIPTS_DIR)
	@$(foreach script,$(XORG_SCRIPTS),\
		$(shell cp $(script) $(SCRIPTS_DIR)))
	@echo "Done."
endif

way-scripts: $(WAY_SCRIPTS)
ifdef WAY_SCRIPTS
	@echo "Installing 'Wayland' scripts (scripts designed for use with Wayland)..."
	@mkdir -p $(SCRIPTS_DIR)
	@$(foreach script,$(WAY_SCRIPTS),\
		$(shell cp $(script) $(SCRIPTS_DIR)))
	@echo "Done."
endif

runit-scripts: $(RUNIT_SCRIPTS)
ifdef RUNIT_SCRIPTS
	@echo "Installing 'runit' scripts (scripts designed for use with the Runit init system)..."
	@mkdir -p $(SCRIPTS_DIR)
	@$(foreach script,$(RUNIT_SCRIPTS),\
		$(shell cp $(script) $(SCRIPTS_DIR)))
	@echo "Done."
endif
