#!/bin/sh
SILENT_FILE="/tmp/notify-sound-silent"
case "$1" in
  "on")
    rm -rf "$SILENT_FILE"
    notify-send "Notification sound on"
    ;;
  "off")
    touch "$SILENT_FILE"
    notify-send "Notification sound off"
    ;;
  *)
    [ ! -f "$SILENT_FILE" ] && paplay ~/.local/share/sounds/notification.flac
    ;;
esac
