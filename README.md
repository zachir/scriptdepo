# ZachIR's Cabal of Scripts

These scripts are provided without any warranty. If you want to use them, you
should copy them out of this directory to a place on your path, as I change them
arbitrarily and on a whim. I don't like to remove functionality; generally my
changes either add something, or just improve POSIX compliance, but either way
you should probably expect at least one of them to functionally change whenever
this repo updates.

That said, I also hope you will find them useful. These are scripts I have
either written, or very substantially modified (i.e. the majority of the source
code is mine). If you care about licensing, these are all licensed under the MIT
license, as are all of my scripts.

If you have suggestions to improve the quality of them, I would be happy to
accept them.

## Scripts

### barmenu

`barmenu` is a script to call either dmenu or bemenu, depending on if Wayland or
X is running. If X is running (`$WAYLAND_DISPLAY` is unset and `$DISPLAY` is),
the script will run dmenu with the provided args. Otherwise, it will run bemenu
with the provided args (albeit replacing -h with -H).

### barmenu_run

`barmenu_run` is a script to replace both dmenu_run and bemenu-run, to call
barmenu instead.

### battery

`battery` is a 'block' for dwmblocks to check the status of the battery, using
`/sys/class/power_supply`. 

### browser

`browser` is a script designed to be used as the default browser, to open links
in one of several browsers listed in the script.

### bsc

`bsc` is a script to select which Basilisk profile to load when opening said
browser. It parses the `profiles.ini` file in the Basilisk config dir. It can be
run with a URL as an argument to open the URL in the selected browser profile.

### bspt

`bspt` is a script to spawn and toggle a terminal scratchpad in bspwm. The first
argument is the class for the scratchpad, and the remaining arguments are passed
as the command to be run in the terminal.

### clock

`clock` is a 'block' for dwmblocks to display the time and date. It can also
show upcoming appointments in calcurse, as well as the calendar for the month.

### cpu

`cpu` is a 'block' for dwmblocks to display the CPU usage. It also shows the top
CPU intensive processes, and can open htop.

### crcparse

`crcparse` is a script to parse values from the `computerrc` file in the user's
config dir.

### disk

`disk` is a 'block' for dwmblocks to display disk usage. It assumes `$location`
will be either `/home` or `/mnt`, but it will work just as well for any loction
(the `$icon` will just be 's').

### dmpv

`dmpv` is a script to use barmenu to load videos from the `~/Videos` directory
into mpv.

### fdc

`fdc` is a script to select which Firedragon profile to load when opening said
browser. It parses the `profiles.ini` file in the Firedragon config dir. It can
be run with a URL as an argument to open the URL in the selected browser
profile.

### ffc

`ffc` is a script to select which Firefox profile to load when opening said
browser. It parse the `profiles.ini` file in the Firefox config dir. It can be
run with a URL as an argument to open the URL in the selected browser profile.

### gettags

`gettags` is a WIP script to read metadata about music files (specifically the
album and artist names). Currently only supports mp3.

### herbsttags

`herbsttags` is a script to format herbstluftwm tags for polybar. This is not
used in the config file presently, but it was before and may one day be again.

### hwinmv

`hwinmv` is a script to both move herbstluftwm windows, and send a signal to
polybar to call herbsttags again to update the bar.

### internet

`internet` is a 'block' for dwmblocks to display the connection status.

### launch_polybar.sh

`launch_polybar.sh` is a script to launch polybar on every monitor, taking the
specific bar in the config file as the first argument.

### liberclip

`liberclip` is a WIP script to paste a URL from the clipboard, and replace it
with a libre/based alternative service (youtube.com gets replaced with yewtu.be;
reddit.com gets replaced with libredd.it). Currently only these two changes are
supported.

### lwc

`lwc` is a script to select which Librewolf profile to load when opening said
browser. It parses the `profiles.ini` file in the Librewolf config dir. It can
be run with a URL as an argument to open the URL in the selected browser
profile.

### memory

`memory` is a 'block' for dwmblocks to show memory usage. It can also open htop,
and show the top memory intensive processes.

### monattach

`monattach` is a WIP script (inspired by `dmenumount`) to attach a monitor to
either the left or right of the primary X monitor. Currently it makes a few side
of the primary monitor, and that the monitor has a resolution of 1920x1080.

### mondetach

`mondetach` is a script (inspired by `dmenuumount`) to remove a currently in use
X monitor using `barmenu`.

### mpdup

`mpdup` is a script to be run as a user-level daemon to update dwmblocks
whenever an mpd event happens.

### mprisctl

`mprisctl` is a script using playerctl and `barmenu` to control MPRIS media
players. The options are:

- pause
- play-pause
- stop
- next
- previous
- position
- volume
- status
- shuffle

### music

`music` is a 'block' for dwmblocks to show mpd status. It can also toggle
play/pause, send a notification with the playing song, and skip forward and
backward in the tracks.

### notify-iptables

`notify-iptables` is a script to send three notifications with firewall status
(iptables, ip6tables, and then nftables).

### notify-sound.sh

`notify-sound.sh` is a script to play a sound with dunst notifications (using my
dunst config). It can also toggle whether to play the sound. It loads the sound
file at `~/.local/share/sounds/notifications.flac`.

### qbc

`qbc` is a script to select which Qutebrowser profile (as generated by qbpm) to
load when opening said browser. It uses qbpm to list the available profiles, and
can be run with a URL as an argument to open the URL in the selected browser
profile.

### sigdwmb

`sigdwmb` is a script to signal dwmblocks to update one or more blocks. It
searches `~/.local/src/dwmblocks/config.h` for each argument, and sends
dwmblocks the respective signals it finds. It can also be run with the argument
"all", in which case it will send every signal in dwmblocks' config.h.

### slockd

`slockd` is a script to open a screen locker, and reload it again if it crashes
or exits unsuccessfully. It will run either slock if in X, or waylock if in
Wayland.

### songgrab

`songgrab` is a script to download, split (if necessary), convert to mp3, and
tag songs from YouTube. It can do this with:

- an individual song in one video,
- a full album in one video,
- a full album in one playlist.

### sortsongs

`sortsongs` is a script to sort a folder of mp3's from various artists/albums
into a folder heirarchy (Artist/Album/Song.mp3).

### startvm

`startvm` is a script to start the runit services for libvirt, then start
virt-manager, and then (after that closes) stop the runit services again.

### startxw

`startxw` is a script to either start the wayland compositor or X window
manager from computerrc, as parsed by `crcparse`, depending on the value of
wayland in computerrc.

### svlogc

`svlogc` is a script to read the log files from runit services in
`/var/log/$SERVICE/current` in less.

### swayidlechk

`swayidlechk` is a script to check if swayidle is running, and return either a
full moon or new moon if it isn't or is running, respectively.

### swayidletog

`swayidletog` is a script to toggle whether swayidle is running.

### tagimg

`tagimg` is a script to tag all mp3 and flac files in a folder hierarchy
(artist/album/song.mp3) with the thumb.jpg in that directory.

### tagmp3

`tagmp3` is a script to guess and then tag an mp3 file.

### todo

`todo` is a script to open in `$EDITOR` the `~/todo.txt` file.

### toggle

`toggle` is a script to kill and restart a process. NOTE: it does not currently
work with Python scripts.

### toggletouchpad

`toggletouchpad` is a script to turn off/on a touchpad in X.

### tsoff

`tsoff` is a script to turn off the touchscreen defined in computerrc.

### tson

`tson` is a script to turn on the touchscreen defined in computerrc.

### volume

`volume` is a 'block' for dwmblocks to display the volume and mute status. It
can also turn the volume up and down, and toggle mute.

### waytoggle

`waytoggle` is a script to toggle whether waybar is running or not.

### xidlechk

`xidlechk` is a 'block' for dwmblocks to display whether the system should be
ready to fall asleep or stay awake (note: if you leave X with the system in
stay awake mode (represented by a full moon), then it will continue to show this
after restarting X, despite not being true).

### xidletog

`xidletog` is a script to toggle whether xorg should stay awake, or be able to
fall asleep (via `xset -dpms` and `xset s off`, and `xset +dpms` and `xset s
on`.) It also signals dwmblocks to update.
